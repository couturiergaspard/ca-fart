# Ca fart
Ça fart est ma premiere tentative de Construction d'une API REST.

Deux dossiers sont à votre disposition.

## Use

Utilisation d'une api pour réaliser une page web dédiée au surf.
Cette page web affichera les conditions maritimes en temps réel ainsi que sur 5 jours.
Les points clefs du surf seront abordés et une "note" recapitulative des conditions sera disponible.

## Create

Une API REST (Representational State Transfer) est une architecture logicielle permettant à des applications de communiquer entre elles via un protocole standardisé, généralement HTTP. Elle est basée sur le principe de l'utilisation des verbes HTTP (GET, POST, PUT, DELETE, etc.) pour effectuer des opérations sur des ressources (par exemple, des données stockées dans une base de données) et de retourner des résultats sous forme de données structurées, telles que JSON ou XML.

Les API REST sont très courantes dans les applications web et mobiles, car elles permettent de fournir des services et des fonctionnalités à d'autres applications de manière efficace et sécurisée. Elles sont également très flexibles et peuvent être utilisées dans une variété de contextes, tels que l'Internet des objets (IoT), les systèmes de gestion de contenu (CMS) et les applications de commerce électronique.

# Projet en cours de réalisation
